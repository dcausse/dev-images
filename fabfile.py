#!/usr/bin/env python2
"""
Usage:

* Install fabric (<http://www.fabfile.org/>) via
  pip install --user fabric or a virtualenv
* Configure your .ssh/config so it can
  access the following hosts and uses
  the proper username and key:
   - contint.wikimedia.org
   - contint1001.wikimedia.org
   - contint2001.wikimedia.org
* Run $ fab deploy_docker

"""
from fabric.api import *  # noqa
from fabric.contrib.console import confirm

env.sudo_prefix = 'sudo -H '
env.use_ssh_config = True


def _update_dev_images():
    with cd('/srv/dev-images'):
        run('git remote update')
        run('git --no-pager log -p HEAD..origin/main dockerfiles')
        if confirm('Does the diff look good?') and confirm(
                'Did you log your reload in #wikimedia-releng '
                '(e.g. "!log Updating dev-images docker-pkg files on primary contint")'):
            run('git rebase')
            run('git -c gc.auto=128 gc --auto --quiet')

            return True

    return False


@task
def deploy_docker():
    """Update docker-pkg built images"""
    env.host_string = 'contint.wikimedia.org'

    updated = _update_dev_images()

    if not updated:
        return

    # docker-pkg does not attempt to pull images - T219398
    run('docker pull docker-registry.wikimedia.org/wikimedia-stretch')
    run('docker pull docker-registry.wikimedia.org/nodejs-devel')

    with cd('/tmp'):
        docker_pkg = '/srv/deployment/docker-pkg/venv/bin/docker-pkg'
        docker_pkg_config = '/etc/docker-pkg/dev-images.yaml'
        dockerfiles = '/srv/dev-images/dockerfiles'
        cmd = '{} --info -c {} build {} | tee dev-images-build.log'.format(
            docker_pkg, docker_pkg_config, dockerfiles)

        run(cmd)

        if confirm('delete build log?'):
            run('rm /tmp/dev-images-build.log')


@task(default=True)
def help():
    """Usage and list of commands"""
    from fabric.main import show_commands
    show_commands(__doc__, 'normal')
